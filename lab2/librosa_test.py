# -*- coding: utf-8 -*-
"""
Created on Wed May 24 17:04:49 2017

@author: Flo
"""
import matplotlib
import matplotlib.pyplot as plt

import os              # access to operating system functions
import glob            # reg-ex based file-iterator

# scientific computing
import numpy as np     # numerical computing
import pandas as pd    # powerful data processing library

# audio feature extraction
import librosa
from rp_extract import rp_extract

# machine learning
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import LinearSVC

from utility_functions import show_classification_results, plot_confusion_matrix, show_query_results

# misc
import itertools
import progressbar

AUDIO_PATH = "D:/projects/air2017-group10/lab2/pop.00000.au"

# 1. Get the file path to the included audio example

# progressbar visualization widget to estimate processing time
pbar = progressbar.ProgressBar()

y, sr = librosa.load(AUDIO_PATH)

# 3. Run the default beat tracker
tempo, beat_frames = librosa.beat.beat_track(y=y, sr=sr)

mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=12)

chroma = librosa.feature.chroma_stft(y=y, sr=sr, n_fft=1024)

rp_features = rp_extract(wavedata = y, 
                              samplerate = sr, 
                              extract_ssd = True, 
                              extract_rp  = True,
                              skip_leadin_fadeout = 0, 
                              verbose     = False)

print(rp_features)


print('Estimated tempo: {:.2f} beats per minute'.format(tempo))

# 4. Convert the frame indices of beat events into timestamps
beat_times = librosa.frames_to_time(beat_frames, sr=sr)
