import matplotlib
import matplotlib.pyplot as plt

import os              # access to operating system functions
import glob            # reg-ex based file-iterator

# scientific computing
import numpy as np     # numerical computing
import pandas as pd    # powerful data processing library

# audio feature extraction
import librosa
from rp_extract import rp_extract

# machine learning
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import LinearSVC

from utility_functions import show_classification_results, plot_confusion_matrix, show_query_results

# misc
import itertools
import progressbar

AUDIO_COLLECTION_PATH = "D:/projects/air2017-group10-additional/lab2/genres_minimized"

print(AUDIO_COLLECTION_PATH)

# fill these lists with values
filenames = []
labels    = []

# add your code here
for label in os.listdir(AUDIO_COLLECTION_PATH):
    for file in os.listdir(AUDIO_COLLECTION_PATH + '/' + label):
        labels.append(label)
        filenames.append(file)
    
print len(labels)
print len(filenames)

# Fill these lists with values
audio_features = {}
audio_features["mfcc"]   = []
audio_features["chroma"] = []
audio_features["ssd"]    = []
audio_features["rp"]     = []

# progressbar visualization widget to estimate processing time
pbar = progressbar.ProgressBar()

# iterate over all files of the collection
for idx, audio_filename in enumerate(pbar(filenames)):
    filepath = AUDIO_COLLECTION_PATH + '/' + labels[idx] + '/' + audio_filename
    
    # 1. load audio
    y, sr = librosa.load(filepath, mono=True)
    
    # 2. extract features
    # - 2.1. mfcc
    #n_mfcc=12, 
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_fft=1024, hop_length=512, n_mfcc=12).T
    
    # - 2.2. chroma
    chroma = librosa.feature.chroma_stft(y=y, sr=sr, n_fft=1024, hop_length=512).T
    
    # -- 2.3. aggregate frame based mfcc and chroma vectors into single feature vectors
    mean_mfccs = np.mean(mfccs, 0)
    std_mfccs = np.std(mfccs, 0)
    
    mfccs_conc = np.concatenate((mean_mfccs, std_mfccs))
    
    mean_chroma = np.mean(chroma, 0)
    std_chroma = np.std(chroma, 0)
    
    chroma_conc = np.concatenate((mean_chroma, std_chroma))
    
    # - 2.4. ssd, rp
    rp_features = rp_extract(wavedata = y, 
                          samplerate = sr, 
                          extract_ssd = True, 
                         extract_rp  = True,
                          skip_leadin_fadeout = 0, 
                          verbose     = False)
    
    # 3. append to provided lists
    audio_features["mfcc"].append(mfccs_conc)
    audio_features["chroma"].append(chroma_conc)
    audio_features["ssd"].append(rp_features["ssd"])
    audio_features["rp"].append(rp_features["rp"])
    
# 4. convert lists of vectors to numpy arrays
audio_features["mfcc"] = np.asarray(audio_features["mfcc"])
audio_features["chroma"] = np.asarray(audio_features["chroma"])
audio_features["ssd"] = np.asarray(audio_features["ssd"])
audio_features["rp"] = np.asarray(audio_features["rp"])

print "Chroma Shape: ",audio_features["chroma"].shape
print "MFCC Shape: ",audio_features["mfcc"].shape
print "SSD Shape: ",audio_features["ssd"].shape
print "RP Shape: ",audio_features["rp"].shape

# create the ground-truth labels
                                 
encoded_labels = []

le = LabelEncoder()

le.fit(labels)

encoded_labels = le.transform(labels)

# fill this dictionary with values
classification_results = {}

pbar = progressbar.ProgressBar()

for feature_name in pbar(audio_features.keys()):
    
    # 1. crossvalidation
    # n_splits = 10, shuffle = True, train = k-1 splits; test = 1 split
    skf = StratifiedKFold(n_splits=10, shuffle=True)
    
    values = []
    
    for train, test in skf.split(audio_features[feature_name], encoded_labels):
    # 2. run cross-validation
        
        # --- TRAIN ---
        
        # 2.1. fit scaler
        training_partition = []
        training_labels = []
        for t in train:
            training_partition.append(audio_features[feature_name][t])
            training_labels.append(encoded_labels[t])
        
        # scale training-partition of features
        scaler = StandardScaler()
        # training_partition = scaler.scale_(training_partition)
        
        # Scale the features to have zero mean and unit variance:
        scaled_features = scaler.fit_transform(training_partition)
        
        # verify that scaling worked
        # [0, 0, 0, 0 .... 0]
        scaled_features.mean(axis=0)
        # [1, 1, 1, 1 .... 1]
        scaled_features.std(axis=0)
        
        # 2.2. create classifier
        # LinearSVC(C=1.0, class_weight=None, dual=True, fit_intercept=True, intercept_scaling=1, loss='squared_hinge', max_iter=1000, multi_class='ovr', penalty='l2', random_state=None, tol=0.0001, verbose=0)
        features = np.vstack(scaled_features)
        model = LinearSVC()
    
        # fit classifier with scaled training-set
        model.fit(features, training_labels)
        
        # --- TEST ---
        
        # 2.3. scale test-partition of features
        testing_partition = []
        testing_labels = []
        for t in test:
            testing_partition.append(audio_features[feature_name][t])
            testing_labels.append(encoded_labels[t])
        
        scaled_test_features = scaler.transform(testing_partition)
        
        # 2.4. use fitted classifier to predict labels of test-partition
        predicted_labels = model.predict(scaled_test_features)
        
        # 2.5. store the true and predicted labels
        values.append(testing_labels)
        values.append(predicted_labels.astype(int).tolist())
        
    # 3. store the classification results back to the dictionary
    classification_results[feature_name] = values
                          
combinations = []

for i in range(1,len(audio_features.keys())):
    combinations.extend(itertools.combinations(audio_features.keys(), i))

# updated evaluation loop with combinations
# fill this dictionary with values
classification_results = {}
combined_audio_features = {}
values = []

pbar = progressbar.ProgressBar()

for feature_combinations in pbar(combinations):
    
    # 1. crossvalidation
    # n_splits = 10, shuffle = True, train = k-1 splits; test = 1 split
    skf = StratifiedKFold(n_splits=10, shuffle=True)
    
    count = len(feature_combinations)
    if count == 1:
        index = feature_combinations[0]
        combined_audio_features[index] = audio_features[feature_combinations[0]]
    if count == 2:
        index = feature_combinations[0] + '_' + feature_combinations[1]
        combined_audio_features[index] = np.concatenate((audio_features[feature_combinations[0]], audio_features[feature_combinations[1]]), axis=1)
    if count == 3:
        index = feature_combinations[0] + '_' + feature_combinations[1] + '_' + feature_combinations[2]
        combined_audio_features[index] = np.concatenate((audio_features[feature_combinations[0]], audio_features[feature_combinations[1]], audio_features[feature_combinations[2]]), axis=1)
        
    for train, test in skf.split(combined_audio_features[index], encoded_labels):
    # 2. run cross-validation
        
        # --- TRAIN ---
        
        # 2.1. fit scaler
        training_partition = []
        training_labels = []
        for t in train:
            training_partition.append(combined_audio_features[index][t])
            training_labels.append(encoded_labels[t])
        
        # scale training-partition of features
        scaler = StandardScaler()
        # training_partition = scaler.scale_(training_partition)
        
        # Scale the features to have zero mean and unit variance:
        scaled_features = scaler.fit_transform(training_partition)
        
        # verify that scaling worked
        # [0, 0, 0, 0 .... 0]
        scaled_features.mean(axis=0)
        # [1, 1, 1, 1 .... 1]
        scaled_features.std(axis=0)
        
        # 2.2. create classifier
        # LinearSVC(C=1.0, class_weight=None, dual=True, fit_intercept=True, intercept_scaling=1, loss='squared_hinge', max_iter=1000, multi_class='ovr', penalty='l2', random_state=None, tol=0.0001, verbose=0)
        features = np.vstack(scaled_features)
        model = LinearSVC()
    
        # fit classifier with scaled training-set
        model.fit(features, training_labels)
        
        # --- TEST ---
        
        # 2.3. scale test-partition of features
        testing_partition = []
        testing_labels = []
        for t in test:
            testing_partition.append(combined_audio_features[index][t])
            testing_labels.append(encoded_labels[t])
        
        scaled_test_features = scaler.transform(testing_partition)
        
        # 2.4. use fitted classifier to predict labels of test-partition
        predicted_labels = model.predict(scaled_test_features)
        
        # 2.5. store the true and predicted labels
        values.append(testing_labels)
        values.append(predicted_labels.astype(int).tolist())
        
    # 3. store the classification results back to the dictionary
    classification_results[index] = values