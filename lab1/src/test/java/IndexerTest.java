import index.Index;
import index.IndexHandling;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class IndexerTest {

    private static final String TREC_DIR = "src\\test\\resources\\input";
    private static final String OUTPUT_FILE = "src\\test\\resources\\index\\index";

    @Test
    public void testWithInputAndOutput_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE};
        Indexer.main(args);
    }

    @Test
    public void testWithInputAndOutput_withCaseFolding_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-cf"};
        Indexer.main(args);
    }

    @Test
    public void testWithInputAndOutput_withCaseFolding_andRemovingStopWords_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-cf", "-rms"};
        Indexer.main(args);
    }

    @Test
    public void testWithInputAndOutput_with_CF_RMS_Lemmatizing_Stemming_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-cf", "-rms", "-l", "-s"};
        Indexer.main(args);
    }

    @Test
    public void testWithInputAndOutput_with_Lemmatizing_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-l"};
        Indexer.main(args);
    }

    @Test
    public void testWithInputAndOutput_with_Lemmatizing_with_MapReduce_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-l", "-mapreduce"};
        Indexer.main(args);
    }

    @Test
    public void testWithInputWithoutOutput_shouldFail() {
        String[] args = {"-input", TREC_DIR};
        Indexer.main(args);
    }

    @Test
    public void testReadingPreviouslyCreatedIndex_shouldSucceed() {
        IndexHandling handling = new IndexHandling();
        Index index = handling.readIndex(OUTPUT_FILE);
        assertThat(index, notNullValue());
    }

    @Test
    public void test_getAvgTermFrequency_shouldSucceed() {
        IndexHandling handling = new IndexHandling();
        Index index = handling.readIndex(OUTPUT_FILE);
        assertThat(index, notNullValue());
        assertThat(index.getMeanAvgTF(), notNullValue());
    }

}
