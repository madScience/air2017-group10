package searcher.scoring;

import index.Index;
import org.junit.Before;
import org.junit.Test;
import parsing.ContentParser;
import parsing.normalizer.LowercaseNormalizer;
import parsing.normalizer.Normalizer;
import parsing.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by cbednar on 23.04.2017.
 * Tests are based on the example of https://janav.wordpress.com/2013/10/27/tf-idf-and-cosine-similarity/
 */
public class BasicCalculationsTest {

    private BasicCalculations calculations;

    private String DOCID1 = "DOC1";
    private String DOCID2 = "DOC2";
    private String DOCID3 = "DOC3";

    @Before
    public void setUp() {
        TestingHelper helper = new TestingHelper();
        calculations = new BasicCalculations(helper.getIndex());

        helper.addToIndex(DOCID1, "The game of life is a game of everlasting learning");
        helper.addToIndex(DOCID2, "The unexamined life is not worth living");
        helper.addToIndex(DOCID3, "Never stop learning");
        helper.getIndex().setTotalDocumentCount(3L);
    }

    @Test
    public void testTf_OnDifferentDocs() {
        assertEquals(2, calculations.tf("game", DOCID1));
        assertEquals(0, calculations.tf("game", DOCID2));
        assertEquals(1, calculations.tf("learning", DOCID3));
    }

    @Test
    public void testTfNormalized_OnDifferentDocs() {
        assertEquals(0.2, calculations.tfNormalized("game", DOCID1), 0);
        assertEquals(0.142857, calculations.tfNormalized("unexamined", DOCID2), 0.000005);
        assertEquals(0.333333, calculations.tfNormalized("learning", DOCID3), 0.000005);
    }

    @Test
    public void testIdf_WordWhichAppearsMoreOften_shouldHaveLowerscore() {
        assertEquals( 2.09861228866811, calculations.idf("game"), 0.000005);
        assertEquals(1.4054651081081644, calculations.idf("learning"), 0.000005);
    }
}
