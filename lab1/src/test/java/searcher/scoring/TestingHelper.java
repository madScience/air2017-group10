package searcher.scoring;

import index.Index;
import parsing.ContentParser;
import parsing.normalizer.LowercaseNormalizer;
import parsing.normalizer.Normalizer;
import parsing.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cbednar on 23.04.2017.
 */
public class TestingHelper {

    private List<Normalizer> normalizers;

    private Tokenizer tokenizer;

    private Index index;

    public TestingHelper() {
        normalizers = new ArrayList<>();
        normalizers.add(new LowercaseNormalizer());
        index = new Index(normalizers);
        tokenizer = new Tokenizer();
    }

    public Index getIndex() {
        return index;
    }

    public void addToIndex(String docId, String content) {
        ContentParser parser = new ContentParser(tokenizer, normalizers);
        List<String> tokens = parser.parse(content);
        Map<String, Integer> wordcount = new HashMap<>();
        for (String token : tokens) {
            Integer count = wordcount.get(token);
            if (count == null) {
                count = 0;
            }
            count++;
            wordcount.put(token, count);
        }

        for (String word : wordcount.keySet()) {
            Integer count = wordcount.get(word);
            index.add(word, docId, count);
        }

        index.addDocumentInfoExtended(docId, tokens.size(), 0d);
    }
}
