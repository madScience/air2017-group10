package searcher.scoring;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Created by cbednar on 23.04.2017.
 */
public class TermFrequencyInverseDocumentFrequencyTest {

    private String DOCID1 = "DOC1";
    private String DOCID2 = "DOC2";
    private String DOCID3 = "DOC3";

    private TermFrequencyInverseDocumentFrequency scoring;

    @Before
    public void setUp() {
        TestingHelper helper = new TestingHelper();
        scoring = new TermFrequencyInverseDocumentFrequency(helper.getIndex());

        helper.addToIndex(DOCID1, "The game of life is a game of everlasting learning");
        helper.addToIndex(DOCID2, "The unexamined life is not worth living");
        helper.addToIndex(DOCID3, "Never stop learning");
        helper.getIndex().setTotalDocumentCount(3L);
    }

    @Test
    public void testSingleTerm() {
        List<ScoreResult> scores = scoring.calculateResults(1, new HashSet<>(Arrays.asList("life")));
        assertNotNull(scores);
        assertEquals(2, scores.size());

        for (ScoreResult result : scores) {
            if (DOCID1.equals(result.getDocumentID())) {
                assertEquals(1.40550715, result.getScore(), 0.00005);
            } else if (DOCID2.equals(result.getDocumentID())) {
                assertEquals(1.4054651081081644, result.getScore(), 0.00005);
            } else {
                fail("Unexpected DocumentID was returned");
            }
        }
    }


}
