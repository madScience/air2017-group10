package parsing.trec;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by cbednar on 06.04.2017.
 */
public class TrecParserTest {

    @Test
    public void test() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File inputFile = new File(classLoader.getResource("input/fb396002").getFile());

            TrecParser parser = new TrecParser(new FileInputStream(inputFile), TrecDocumentParser.ENCODING);
            int countDocs = 0;
            Map<String, Object> parsed = null;
            do {
                parsed = parser.next_doc();
                countDocs++;
            } while(parsed != null);
            assertEquals(34, countDocs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
