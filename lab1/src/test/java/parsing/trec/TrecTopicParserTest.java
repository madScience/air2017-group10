package parsing.trec;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TrecTopicParserTest {

    @Test
    public void test() {
        try {
            String testfilename = "topics\\topicsTREC8Adhoc.txt";
            Path inputFile = Paths.get(ClassLoader.getSystemResource(testfilename).toURI());

            TrecTopicParser parser = new TrecTopicParser();
            List<TrecTopic> parsed = parser.parse(inputFile);
            assertNotNull(parsed);
            assertEquals(50, parsed.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
