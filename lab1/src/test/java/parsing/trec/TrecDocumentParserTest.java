package parsing.trec;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * Created by cbednar on 05.04.2017.
 */
public class TrecDocumentParserTest {

    @Test
    public void test() {
        try {
            String testfilename = "ft922_4";
            Path inputFile = Paths.get(ClassLoader.getSystemResource(testfilename).toURI());

            TrecDocumentParser parser = new TrecDocumentParser();
            List<TrecDocument> parsed = parser.parse(inputFile);
            assertNotNull(parsed);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
