package parsing.normalizer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by cbednar on 25.04.2017.
 */
public class LemNormalizerTest {

    private Normalizer normalizer;

    @Before
    public void setUp() {
        normalizer = new LemNormalizer();
    }

    @Test
    public void testLemmatization() {
        assertEquals("car", normalizer.normalize("car's"));
        assertEquals("be", normalizer.normalize("are"));
        assertEquals("be", normalizer.normalize("is"));
    }
}
