package parsing.normalizer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by cbednar on 07.04.2017.
 */
public class StemNormalizerTest {

    private Normalizer normalizer;

    @Before
    public void setUp() {
        normalizer = new StemNormalizer();
    }

    @Test
    public void testStemming() {
        assertEquals("librari", normalizer.normalize("libraries"));
        assertEquals("librari", normalizer.normalize("library"));
        assertEquals("Wiki", normalizer.normalize("Wikis"));
    }
}
