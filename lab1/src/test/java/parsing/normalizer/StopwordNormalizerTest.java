package parsing.normalizer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by cbednar on 07.04.2017.
 */
public class StopwordNormalizerTest {

    private Normalizer normalizer;

    @Before
    public void setUp() {
        normalizer = new StopwordNormalizer();
    }

    @Test
    public void testWithWordInStopword_shouldReturnNull() {
        assertNull(normalizer.normalize("a"));
    }

    @Test
    public void testWithWordNotInStopword_shouldReturnWord() {
        assertEquals("giraffe", normalizer.normalize("giraffe"));
    }
}
