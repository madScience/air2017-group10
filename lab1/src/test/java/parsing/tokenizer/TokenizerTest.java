package parsing.tokenizer;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by cbednar on 07.04.2017.
 */
public class TokenizerTest {

    private Tokenizer tokenizer;

    @Before
    public void setUp() {
        tokenizer = new Tokenizer();
    }

    @Test
    public void test() {
        String text = "The government\n" +
                "yesterday announced that Mr Bernard Pache, the chairman of Charbonnages de\n" +
                "France, would succeed Mr Francis Lorentz as Bull's chief.\n" +
                "The government wants to ensure management continuity where possible. It\n" +
                "needs to avoid worrying other shareholders while it is selling minority\n" +
                "stakes in state companies to fund its employment policies. It also needs to\n" +
                "be cautious because many state-controlled groups are forging alliances with\n" +
                "foreign partners.";
        List<String> tokens = tokenizer.tokenize(text);
        assertNotNull(tokens);

    }
}
