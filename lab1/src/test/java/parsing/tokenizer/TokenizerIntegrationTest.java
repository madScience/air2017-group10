package parsing.tokenizer;

import org.junit.Test;
import parsing.trec.TrecDocument;
import parsing.trec.TrecDocumentParser;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * Created by cbednar on 23.04.2017.
 */
public class TokenizerIntegrationTest {

    private TrecDocumentParser documentParser = new TrecDocumentParser();
    private Tokenizer tokenizer = new Tokenizer();

    @Test
    public void testParsingAndTokenizing() throws IOException {
        Path p = Paths.get("src/test/resources/input/fb396010");
        List<TrecDocument> docs = documentParser.parse(p);
        for(TrecDocument doc : docs) {
            List<String> tokens = tokenizer.tokenize(doc.getText());
            assertNotNull(tokens);
        }
    }
}
