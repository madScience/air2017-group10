import org.junit.Test;

public class SearcherTest {

    private static final String INDEX_FILE = "src\\test\\resources\\index\\index";
    private static final String TOPICS_FILE = "src\\test\\resources\\topics\\topicsTREC8Adhoc.txt";

    private static String OUTPUT_PATH = "src\\test\\resources\\rankings\\";

    @Test
    public void testTFIDF_withTopicFile_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "TF-IDF", "-i", INDEX_FILE, "-o", OUTPUT_PATH + "rankings_TFIDF.txt"};
        Searcher.main(args);
    }

    @Test
    public void testTFIDFFrequency_withoutTopicFile_shouldFail() {
        String[] args = {"-sf", "TF-IDF", "-i", INDEX_FILE};
        Searcher.main(args);
    }

    @Test
    public void testBM25_withTopicFile_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "BM25", "-i", INDEX_FILE, "-o", OUTPUT_PATH + "rankings_BM25.txt"};
        Searcher.main(args);
    }

    @Test
    public void test_withInvalidScoringFunction_shouldFail() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "BM30", "-i", INDEX_FILE};
        Searcher.main(args);
    }

    @Test
    public void testBM25VA_withTopicFile_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "BM25VA", "-i", INDEX_FILE, "-o", OUTPUT_PATH + "rankings_BM25VA.txt"};
        Searcher.main(args);
    }
}
