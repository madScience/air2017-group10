import index.Index;
import index.IndexHandling;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parsing.normalizer.Normalizer;
import searcher.scoring.BasicCalculations;
import searcher.scoring.ScoreHelper;

import java.util.*;


public class ReportTest {

    private static final String TOPICS_FILE = "src\\test\\resources\\test\\testQuery.txt";
    private static final String INDEX_FILE = "src\\test\\resources\\index\\index";

    private static final String TREC_DIR = "src\\test\\resources\\input";
    private static final String OUTPUT_FILE = "src\\test\\resources\\index\\index";

    private static final String[] queryWords = {"test", "query", "with", "random", "words"};
    private static final String[] docs = {"FBIS3-300", "FBIS3-191"};

    private static Logger LOGGER = LoggerFactory.getLogger(ReportTest.class);

    @Test
    public void testCreateIndex_forReportQuery_shouldSucceed() {
        String[] args = {"-input", TREC_DIR, "-output", OUTPUT_FILE, "-l"};
        Indexer.main(args);
    }

    @Test
    public void testTFIDF_withOwnQuery_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "TF-IDF", "-i", INDEX_FILE, "-o", "src\\test\\resources\\test\\rankings_TFIDF"};
        Searcher.main(args);

        // TOP two documents by distinct score: FBIS3-300 & FBIS3-191
        IndexHandling handling = new IndexHandling();
        Index index = handling.readIndex(INDEX_FILE);

        LOGGER.info("Normalizing the query words ...");

        List<String> tokens = new ArrayList<>(Arrays.asList(queryWords));
        Set<String> query = normalizedTokens(index.getNormalizers(), tokens);

        LOGGER.info("Calculating the Score for document FBIS3-300 and FBIS3-191...");

        BasicCalculations calc = new BasicCalculations(index);
        Set<String> docs = new HashSet<>(Arrays.asList(ReportTest.docs));

        Map<String, Double> idfCache = new HashMap<>();

        for (String token : query) {
            for (String doc : docs) {
                double tfNormalized = calc.tfNormalized(token, doc);
                double idf = calc.idf(token);
                double tfIdfCalculated = tfNormalized * idf;
                LOGGER.info("Token \"" + token + "\" and doc \"" + doc + "\" => TF: " + tfNormalized + " IDF: " + idf + "   TF-IDF = " + tfIdfCalculated);

                Double tfidf = idfCache.get(doc);
                if (tfidf == null) {
                    tfidf = 0d;
                }
                tfidf += tfIdfCalculated;
                idfCache.put(doc, tfidf);
            }
        }

        LOGGER.info("Results for FBIS3-300: " + idfCache.get("FBIS3-300"));
        LOGGER.info("Results for FBIS3-191: " + idfCache.get("FBIS3-191"));
    }

    @Test
    public void testBM25_withOwnQuery_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "BM25", "-i", INDEX_FILE, "-o", "src\\test\\resources\\test\\rankings_BM25"};
        Searcher.main(args);

        // TOP two documents by distinct score: FBIS3-300 & FBIS3-191
        IndexHandling handling = new IndexHandling();
        Index index = handling.readIndex(INDEX_FILE);

        LOGGER.info("Normalizing the query words ...");

        List<String> tokens = new ArrayList<>(Arrays.asList(queryWords));
        Set<String> query = normalizedTokens(index.getNormalizers(), tokens);

        ScoreHelper helper = new ScoreHelper(index);

        Set<String> validDocuments = new HashSet<>(Arrays.asList(ReportTest.docs));

        double avgDocLength = index.getAvgDocumentLength();

        for (String doc : validDocuments) {
            double topicScore;
            double b = 0.75;
            double k = 1.5;
            int docLength = index.getDocumentLength(doc);
            double docLengthWeight = docLength / avgDocLength;

            LOGGER.info("BM25 score is being calculated for document " + doc + "...");
            LOGGER.info("Concrete values as input to the formula: b = " + b + ", k = " + k + ", document length: " + docLength +
                    ", average document length = " + avgDocLength + ", document length weight: " + docLengthWeight);
            topicScore = helper.calculateTopicScore(query, doc, k, b, docLengthWeight);
            LOGGER.info("Resulting topic score for document " + doc + " is " + topicScore);
        }
    }

    @Test
    public void testBM25VA_withOwnQuery_shouldSucceed() {
        String[] args = {"-f", TOPICS_FILE, "-sf", "BM25VA", "-i", INDEX_FILE, "-o", "src\\test\\resources\\test\\rankings_BM25VA"};
        Searcher.main(args);

        // TOP two documents by distinct score: FBIS3-300 & FBIS3-191
        IndexHandling handling = new IndexHandling();
        Index index = handling.readIndex(INDEX_FILE);

        LOGGER.info("Normalizing the query words ...");

        List<String> tokens = new ArrayList<>(Arrays.asList(queryWords));
        Set<String> query = normalizedTokens(index.getNormalizers(), tokens);

        ScoreHelper helper = new ScoreHelper(index);

        Set<String> validDocuments = new HashSet<>(Arrays.asList(ReportTest.docs));

        double avgDocLength = index.getAvgDocumentLength();
        double mavgtf = index.getMeanAvgTF();

        for (String doc : validDocuments) {
            double topicScore;
            double ld = index.getDocTF(doc);
            int docLength = index.getDocumentLength(doc);
            double docLengthWeight = docLength / avgDocLength;
            double k = 1.2;

            LOGGER.info("BM25VA uses different values for b to take the distribution of terms of a document into account...");
            LOGGER.info("bVA score is being calculated using the formula provided in the paper...");
            double bVA = Math.pow(mavgtf, -2) * ld/docLength + (1 - Math.pow(mavgtf, -1)) * ld / avgDocLength;

            LOGGER.info("BM25VA score is being calculated for document " + doc + "...");
            LOGGER.info("Concrete values as input to the formula: bVA = " + bVA + ", k = " + k + ", document length: " + docLength +
                    ", average document length = " + avgDocLength + ", document length weight: " + docLengthWeight);
            topicScore = helper.calculateTopicScore(query, doc, k, bVA, docLengthWeight);
            LOGGER.info("Resulting topic score for document " + doc + " is " + topicScore);
        }
    }

    private Set<String> normalizedTokens (List<Normalizer> normalizers, List<String> tokens) {
        Set<String> normalizedTokens = new HashSet<>();
        for (String token : tokens) {
            if (normalizers != null) {
                for (Normalizer normalizer : normalizers) {
                    token = normalizer.normalize(token);
                }
            }
            if (token != null) {
                normalizedTokens.add(token);
            }
        }
        return normalizedTokens;
    }
}
