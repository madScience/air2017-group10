@ECHO OFF
set CLASSPATH=.
set CLASSPATH=%CLASSPATH%;
REM path/to/needed/jars/my.jar

REM set INPUTPATH=C:\git\air2017-group10\src\test\resources\input
set INPUTPATH=C:\Users\cbednar\Downloads\TREC8all\TREC8all\TREC8Adhoc.tar\TREC8Adhoc\Adhoc
set OUTPUTPATH=C:\git\air2017-group10\src\test\resources\temp\stemmingindex.index

%JAVA_HOME%/bin/java -Xmx8g -jar ././../../../../target/air2017-group10-1.0-jar-with-dependencies.jar indexer -i %INPUTPATH% -o %OUTPUTPATH% -cf -rms -s