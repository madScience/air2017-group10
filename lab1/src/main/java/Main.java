import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        if (Objects.equals(args[0], "indexer")) {
            Indexer.main(args);
        } else {
            Searcher.main(args);
        }
    }
}
