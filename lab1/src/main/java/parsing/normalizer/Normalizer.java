package parsing.normalizer;

import java.io.Serializable;

/**
 * Created by cbednar on 07.04.2017.
 */
public interface Normalizer extends Serializable {

    /**
     * Runs normalization technique on a single word
     *
     * @param word
     * @return the normalized value or NULL if the value shouldn't be used anymore (because removed or something)
     */
    String normalize(String word);

}
