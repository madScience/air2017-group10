package parsing.normalizer;

import edu.stanford.nlp.simple.Sentence;

/**
 * Created by cbednar on 07.04.2017.
 */
public class LemNormalizer implements Normalizer {
    @Override
    public String normalize(String word) {
        if(word == null) {
            return null;
        }
        return new Sentence(word).lemma(0);
    }
}
