package parsing.normalizer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by cbednar on 07.04.2017.
 */
public class StopwordNormalizer implements Normalizer {

    private Set<String> stopwords = new HashSet<>(Arrays.asList(
            new String[]{"a", "an", "and", "are", "as", "at", "be", "but", "by", "for",
                    "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such",
                    "that", "the", "their", "then", "there", "these",
                    "they", "this", "to", "was", "will", "with"}
    ));

    @Override
    public String normalize(String word) {
        if (stopwords.contains(word)) {
            return null;
        } else {
            return word;
        }
    }
}
