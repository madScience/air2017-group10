package parsing.normalizer;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by cbednar on 07.04.2017.
 */
public class StemNormalizer implements Normalizer {

    @Override
    public String normalize(String word) {
        if(word == null || StringUtils.isEmpty(word)) {
            return null;
        }
        PorterStemmer stemmer = new PorterStemmer();
        stemmer.add(word.toCharArray(), word.length());
        stemmer.stem();
        return stemmer.toString();
    }
}
