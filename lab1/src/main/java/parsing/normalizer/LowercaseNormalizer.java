package parsing.normalizer;

/**
 * Created by cbednar on 07.04.2017.
 */
public class LowercaseNormalizer implements Normalizer {

    @Override
    public String normalize(String word) {
        if (word == null) {
            return null;
        }
        return word.toLowerCase();
    }

}
