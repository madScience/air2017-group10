package parsing;

import org.apache.commons.cli.*;
import parsing.normalizer.*;

import java.util.*;

/**
 * Created by cbednar on 07.04.2017.
 */
public class CommandLineHandling {

    public static final String CASE_FOLDING = "case-folding";
    public static final String REMOVE_STOP_WORDS = "remove-stop-words";
    public static final String STEMMING = "stem";
    public static final String LEMMATIZE = "lemmatize";
    public static final String MAPREDUCE = "mapreduce";

    public CommandLine parseArguments(Options options, String[] args) {
        // create the parser
        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            return line;
        } catch (ParseException exp) {
            // automatically generate the help statement
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("indexer", options);
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
            return null;
        }
    }

    public List<Normalizer> defineNormalizers(CommandLine line) {
        List<Normalizer> normalizers = new ArrayList<>();
        if(line.hasOption(CASE_FOLDING)) {
            normalizers.add(new LowercaseNormalizer());
        }
        if(line.hasOption(REMOVE_STOP_WORDS)) {
            normalizers.add(new StopwordNormalizer());
        }
        if(line.hasOption(STEMMING)) {
            normalizers.add(new StemNormalizer());
        }
        if(line.hasOption(LEMMATIZE)) {
            normalizers.add(new LemNormalizer());
        }
        return normalizers;
    }
}
