package parsing.trec;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.benchmark.quality.trec.TrecTopicsReader;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cbednar on 08.04.2017.
 */
public class TrecTopicParser implements Serializable {

    public static final Charset ENCODING = Charset.forName("ISO-8859-1");

    public List<TrecTopic> parse(Path path) throws IOException {
        List<TrecTopic> topics = new ArrayList<>();

        TrecTopicsReader reader = new TrecTopicsReader();
        QualityQuery[] queries = reader.readQueries(Files.newBufferedReader(path, ENCODING));
        for(QualityQuery query : queries) {
            topics.add(new TrecTopic(
                    Integer.parseInt(query.getQueryID()),
                    query.getValue("title"),
                    query.getValue("description"),
                    query.getValue("narrative")
            ));
        }
        return topics;
    }
}
