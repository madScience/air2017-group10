package parsing.trec;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cbednar on 05.04.2017.
 */
public class TrecDocumentParser implements Serializable {

    public static final String ENCODING = "ISO-8859-1";

    public static final String DOCNO = "DOCNO";
    public static final String TEXT = "CONTENT";

    public List<TrecDocument> parse(Path path) throws IOException {
        List<TrecDocument> docs = new ArrayList<>();

        TrecParser parser = new TrecParser(Files.newInputStream(path), ENCODING);
        Map<String, Object> parsed = null;
        do {
            parsed = parser.next_doc();
            if(parsed != null) {
                Object docNoRaw = parsed.get(DOCNO);
                Object textRaw = parsed.get(TEXT);

                if(docNoRaw != null && textRaw != null) {
                    String docNo = (String) docNoRaw;
                    String text = new String((char[]) textRaw);
                    if (!StringUtils.isEmpty(docNo) && !StringUtils.isEmpty(text)) {
                        docs.add(new TrecDocument(docNo, text));
                    }
                }
            }
        } while (parsed != null);
        return docs;
    }

}
