package parsing.trec;

import java.util.Comparator;

/**
 * Created by cbednar on 08.04.2017.
 */
public class TrecTopic {

    private int topicId;

    private String title;

    private String description;

    private String narrative;

    public TrecTopic(int topicId, String title, String description, String narrative) {
        this.topicId = topicId;
        this.title = title;
        this.description = description;
        this.narrative = narrative;
    }

    public int getTopicId() {
        return topicId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getNarrative() {
        return narrative;
    }

    public static class TopicIdComparator implements Comparator<TrecTopic> {
        @Override
        public int compare(TrecTopic o1, TrecTopic o2) {
            return Integer.compare(o1.getTopicId(), o2.getTopicId());
        }

    }
}
