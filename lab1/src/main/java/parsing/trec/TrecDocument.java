package parsing.trec;

/**
 * Created by cbednar on 05.04.2017.
 */
public class TrecDocument {

    private String docNo;

    private String text;

    public TrecDocument(String docNo, String text) {
        this.docNo = docNo;
        this.text = text;
    }

    public String getDocNo() {
        return docNo;
    }

    public String getText() {
        return text;
    }
}
