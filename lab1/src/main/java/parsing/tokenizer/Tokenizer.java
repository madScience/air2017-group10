package parsing.tokenizer;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbednar on 07.04.2017.
 */
public class Tokenizer implements Serializable {

    public List<String> tokenize(String input) {
        List<String> tokenized = new ArrayList<>();
        String[] splitted = input.split("\\W+");
        for (String word : splitted) {
            if(StringUtils.isNoneBlank(word)) {
                tokenized.add(word);
            }
        }
        return tokenized;
    }

}
