package parsing;

import parsing.normalizer.Normalizer;
import parsing.tokenizer.Tokenizer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cbednar on 23.04.2017.
 */
public class ContentParser implements Serializable {

    private Tokenizer tokenizer;

    private List<Normalizer> normalizers;

    public ContentParser(Tokenizer tokenizer, List<Normalizer> normalizers) {
        this.tokenizer = tokenizer;
        this.normalizers = normalizers;
    }

    public List<String> parse(String content) {
        List<String> tokens = tokenizer.tokenize(content);
        List<String> normalizedTokens = new ArrayList<>();
        for (String token : tokens) {
            if (normalizers != null) {
                for (Normalizer normalizer : normalizers) {
                    token = normalizer.normalize(token);
                }
            }
            if (token != null) {
                normalizedTokens.add(token);
            }
        }
        return normalizedTokens;
    }
}
