import index.Index;
import index.IndexHandling;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import parsing.ContentParser;
import parsing.tokenizer.Tokenizer;
import parsing.trec.TrecTopic;
import parsing.trec.TrecTopicParser;
import searcher.CommandLineHandling;
import searcher.scoring.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.*;

public class Searcher {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(Searcher.class);

    private static final String OPTION_HELP = "help";
    private static final String OPTION_FILE = "file";
    private static final String OPTION_SCORING_FUNCTION = "scoring_function";
    private static final String OPTION_OUTPUT = "output";

    private static final String DEFAULT_INDEX_PATH = "src\\main\\resources\\index\\index";

    private static ScoreFunction sf;

    public static void main(String[] args) {
        CommandLineHandling cliHandlingSearcher = new CommandLineHandling();
        Options opts = createOptions();
        CommandLine line = cliHandlingSearcher.parseArguments(opts, args);
        if (line == null) {
            return;
        }
        if (line.hasOption(OPTION_HELP)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("searcher", opts);
            return;
        }

        IndexHandling handling = new IndexHandling();
        String indexFile;
        Index index;
        LOGGER.info("Reading index");
        if (line.hasOption(CommandLineHandling.INDEX)) {
            indexFile = line.getOptionValue(CommandLineHandling.INDEX);
            index = handling.readIndex(indexFile);
        } else {
            index = handling.readIndex(DEFAULT_INDEX_PATH);
        }
        LOGGER.info("Finished reading index");
        if (index != null) {
            String scoringFunction = line.getOptionValue(OPTION_SCORING_FUNCTION);
            LOGGER.info("Setting scoring function to " + scoringFunction);
            switch (line.getOptionValue(OPTION_SCORING_FUNCTION)) {
                case ("TF-IDF"):
                    sf = new TermFrequencyInverseDocumentFrequency(index);
                    break;
                case ("BM25"):
                    sf = new BestMatching25(index);
                    break;
                case ("BM25VA"):
                    sf = new BestMatching25VA(index);
                    break;
                default:
                    LOGGER.error("Scoring function is invalid!");
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp("searcher", opts);
                    return;
            }

            ContentParser contentParser = new ContentParser(new Tokenizer(), index.getNormalizers());
            try {
                TrecTopicParser topicParser = new TrecTopicParser();
                String topicfilePath = line.getOptionValue(OPTION_FILE);
                LOGGER.info("Start parsing topicfiles from: " + topicfilePath);
                List<TrecTopic> trecTopics = topicParser.parse(Paths.get(topicfilePath));
                LOGGER.info("Number of selected topics: " + trecTopics.size());

                List<ScoreResult> allResults = new ArrayList<>();

                trecTopics.sort(new TrecTopic.TopicIdComparator());
                for (TrecTopic trecTopic : trecTopics) {
                    Set<String> query = createQuery(trecTopic, contentParser);
                    List<ScoreResult> scoreResults = sf.getScoreResults(trecTopic.getTopicId(), query);
                    scoreResults.sort(new ScoreResult.ScoreComparator());
                    scoreResults = scoreResults.subList(0, 1000 > scoreResults.size() ? scoreResults.size() : 1000);
                    for (int i = 0; i < scoreResults.size(); i++) {
                        scoreResults.get(i).setRank(i + 1);
                    }
                    allResults.addAll(scoreResults);
                }
                printResults(allResults);
                String outputPath = line.getOptionValue(OPTION_OUTPUT);
                if (outputPath != null) {
                    printToFile(allResults, outputPath);
                }
            } catch (IOException e) {
                LOGGER.error("IO-Error on reading files", e);
            }
        }

    }

    private static Set<String> createQuery(TrecTopic trecTopic, ContentParser parser) {
        String desc = trecTopic.getDescription();
        String title = trecTopic.getTitle();
        return new HashSet<>(parser.parse(desc + " " + title));
    }

    private static void printResults(List<ScoreResult> scoreResults) {
        if (scoreResults != null && scoreResults.size() != 0) {
            /*for (ScoreResult scoreResult : scoreResults) {
                System.out.println(scoreResult);
            }*/
            LOGGER.info(scoreResults.size() + " scoring results created");
        }
        LOGGER.info("Runtime duration of scoring function in ms: " + sf.getDuration());

    }

    private static void printToFile(List<ScoreResult> scoreResults, String path) {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter(path));
            for (ScoreResult res : scoreResults) {
                pw.println(res.toString());
            }
        } catch (IOException e) {

        } finally {
            if (pw != null) {
                pw.close();
            }
        }


    }

    private static Options createOptions() {
        Options options = new Options();
        options.addOption("h", "help", false, "prints help message");
        options.addRequiredOption("f", OPTION_FILE, true, "the input file containing the topic to search for");
        options.addRequiredOption("sf", OPTION_SCORING_FUNCTION, true, "the scoring function that has to be used by the engine (possible values are TF-IDF, BM25 and BM25VA)");

        options.addOption("i", CommandLineHandling.INDEX, true, "file containing the previously generated index");
        options.addOption("o", OPTION_OUTPUT, true, "outputs searchranking to this file");
        return options;
    }
}
