package index;

import parsing.normalizer.Normalizer;

import java.io.Serializable;
import java.util.*;

public class Index implements Serializable {

    private List<Normalizer> normalizers = new ArrayList<>();

    /* N */
    private Long totalDocumentCount;

    private Map<String, Map<String, Integer>> dictionary = new HashMap<>();

    private Map<String, Collection<Object>> documentInfoExtended = new HashMap<>();

    public Index(List<Normalizer> normalizers) {
        this.normalizers = normalizers;
    }

    public void setTotalDocumentCount(Long totalDocumentCount) {
        this.totalDocumentCount = totalDocumentCount;
    }

    public Long getTotalDocumentCount() {
        return totalDocumentCount;
    }

    public void add(String word, String doc, int tf) {
        Map<String, Integer> docAppearences = dictionary.get(word);
        if(docAppearences == null) {
            docAppearences = new HashMap<>();
            dictionary.put(word, docAppearences);
        }
        docAppearences.put(doc, tf);
    }

    /** n(qi) */
    public int getNumberOfDocumentsContainingWord(String word) {
        Map<String, Integer> set = dictionary.get(word);
        if(set == null) {
            return 0;
        }
        return set.size();
    }

    public Set<String> getDocumentsContainingWord(String word) {
        Map<String, Integer> docs = new HashMap<>();
        if (word != null) {
            docs = dictionary.get(word);
        }
        if (docs != null)
            return docs.keySet();
        else
            return null;
    }

    public int getFrequenciesOfWordInDocument(String word, String doc) {
        Map<String, Integer> docs;
        if (word != null) {
            docs = dictionary.get(word);
            if (docs != null && docs.containsKey(doc))
                return docs.get(doc);
        }
        return 0;
    }

    public List<Normalizer> getNormalizers() {
        return normalizers;
    }


    public int getDocumentLength(String docID) {
        Collection<Object> values = documentInfoExtended.getOrDefault(docID, new ArrayList<>());
        if (values.isEmpty()) {
            return 0;
        } else {
            return (int) ((ArrayList<Object>) values).get(0);
        }
    }
    /*
    public void addDocumentInfo(String docID, int wordCount) {
        if (!documentInfo.containsKey(docID)) {
            documentInfo.put(docID, wordCount);
        }
    } */

    public void addDocumentInfoExtended(String docID, int wordCount, double sumTf) {
        Collection<Object> values = new ArrayList<>();
        values.add(wordCount);
        values.add(sumTf);
        if (!documentInfoExtended.containsKey(docID)) {
            documentInfoExtended.put(docID, values);
        }
    }

    public double getAvgDocumentLength() {
        int sumLength = 0;

        for (Map.Entry entry : documentInfoExtended.entrySet()) {
            sumLength +=  (int) ((ArrayList<Object>) entry.getValue()).get(0);
        }

        return sumLength / documentInfoExtended.size();
    }

    public double getDocTF(String docID) {
        if (documentInfoExtended.containsKey(docID)) {
            return (double) ((ArrayList<Object>) documentInfoExtended.get(docID)).get(1);
        } else {
            return 0;
        }
    }

    public double getMeanAvgTF() {
        double sumTF = 0;

        for (Map.Entry entry : documentInfoExtended.entrySet()) {
            List<Object> values = (ArrayList<Object>) entry.getValue();
            sumTF += (double) values.get(1);
        }

        return sumTF / totalDocumentCount;
    }
}
