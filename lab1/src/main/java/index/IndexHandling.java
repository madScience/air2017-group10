package index;

import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.FSTObjectInput;
import org.nustaq.serialization.FSTObjectOutput;
import org.slf4j.Logger;

import java.io.*;

/**
 * Created by cbednar on 07.04.2017.
 */
public class IndexHandling {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(IndexHandling.class);

    public Index readIndex(String filepath) {
        InputStream input = null;
        LOGGER.info("Started reading index");
        try {
            input = new FileInputStream(filepath);
            FSTObjectInput fst = null;
            try {
                fst = new FSTObjectInput(input);
                Index result = (Index) fst.readObject();
                LOGGER.debug("Index reading completed");
                return result;
            } finally {
                if (fst != null) {
                    fst.close();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error("Error on reading Index", e);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e1) {
                LOGGER.error("Wasn't able to close Index-reader", e1);
            }
        }
        LOGGER.info("Finished reading index");
        return null;
    }

    public void writeIndex(Index index, String filepath) {
        LOGGER.info("Start writing index");
        OutputStream out = null;
        try {
            out = new FileOutputStream(filepath);
            FSTObjectOutput fst = null;
            try {
                fst = new FSTObjectOutput(out);
                fst.writeObject(index);
            } catch (IOException e1) {
                LOGGER.error("Error on writing index object", e1);
            } finally {
                if (fst != null) {
                    fst.close(); // required !
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error on writing index object", e);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    LOGGER.error("Error on writing index object", e);
                }
            }
        }
        LOGGER.info("Finished writing index");
    }
}
