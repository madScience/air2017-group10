package searcher.scoring;

import index.Index;
import org.slf4j.Logger;

import java.util.*;

public class TermFrequencyInverseDocumentFrequency implements ScoreFunction {

    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(TermFrequencyInverseDocumentFrequency.class);

    private Map<String, Double> idfCache = new HashMap<>();

    private long startTime;
    private long endTime;

    private Index index;
    private BasicCalculations calc;

    public TermFrequencyInverseDocumentFrequency(Index index) {
        this.index = index;
        this.calc = new BasicCalculations(index);
    }

    /**
     * Score is calculated by summing up the TF-IDF per term for all found documents
     * https://nlp.stanford.edu/IR-book/html/htmledition/tf-idf-weighting-1.html
     *
     * @return
     */
    @Override
    public List<ScoreResult> getScoreResults(int topicId, Set<String> query) {
        startTime = new Date().getTime();

        List<ScoreResult> scoreResults = new ArrayList<>();
        scoreResults.addAll(calculateResults(topicId, query));

        endTime = new Date().getTime();
        return scoreResults;
    }

    protected List<ScoreResult> calculateResults(int topicId, Set<String> tokens) {
        Map<String, Double> tfIdfPerDocId = new HashMap<>();
        for (String word : tokens) {
            Set<String> foundDocs = index.getDocumentsContainingWord(word);
            if (foundDocs != null && foundDocs.size() != 0) {
                for (String doc : foundDocs) {
                    double tfidfPerTerm = calc.tf(word, doc) * idf(word, calc);
                    //LOGGER.debug("TF-IDF for word/doc/tfidf: " + word + "/" + doc + "/" + tfidfPerTerm);
                    sumUp(tfIdfPerDocId, doc, tfidfPerTerm);
                }
            }
        }

        List<ScoreResult> scoreResults = new ArrayList<>();
        for (Map.Entry<String, Double> entrySet : tfIdfPerDocId.entrySet()) {
            ScoreResult result = new ScoreResult();
            result.setTopicID(topicId);
            result.setDocumentID(entrySet.getKey().toString());
            result.setScore(entrySet.getValue());
            scoreResults.add(result);
        }
        return scoreResults;
    }

    private void sumUp(Map<String, Double> tfIdfPerDocId, String doc, double tfidfPerTerm) {
        Double tfidf = tfIdfPerDocId.get(doc);
        if (tfidf == null) {
            tfidf = 0d;
        }
        tfidf += tfidfPerTerm;
        tfIdfPerDocId.put(doc, tfidf);
    }

    /**
     * Caches the already calculated idfs for performance
     *
     * @param word
     * @param calculations
     * @return
     */
    private double idf(String word, BasicCalculations calculations) {
        Double idf = idfCache.get(word);
        if (idf == null) {
            idf = calculations.idf(word);
            idfCache.put(word, idf);
        }
        return idf;
    }

    @Override
    public long getDuration() {
        return endTime - startTime;
    }
}
