package searcher.scoring;

import index.Index;
import parsing.ContentParser;
import parsing.tokenizer.Tokenizer;
import parsing.trec.TrecTopic;

import java.util.*;

public class BestMatching25VA implements ScoreFunction {

    private long startTime;
    private long endTime;

    private Index index;
    private ContentParser parser;
    private ScoreHelper helper;

    public BestMatching25VA(Index index) {
        this.index = index;
        Tokenizer tokenizer = new Tokenizer();
        this.parser = new ContentParser(tokenizer, index.getNormalizers());
        this.helper = new ScoreHelper(index);
    }


    @Override
    public List<ScoreResult> getScoreResults(int topicId, Set<String> words) {
        startTime = new Date().getTime();

        List<ScoreResult> scoreResults = new ArrayList<>();
        double mavgtf = index.getMeanAvgTF();
        double avgDocLength = index.getAvgDocumentLength();

        Set<String> validDocuments = helper.getValidDocuments(words);

        for (String doc : validDocuments) {
            ScoreResult result = new ScoreResult();
            result.setTopicID(topicId);
            result.setDocumentID(doc);

            double topicScore;
            double ld = index.getDocTF(doc);
            int docLength = index.getDocumentLength(doc);
            double bVA = calculateBVA(mavgtf, ld, avgDocLength, docLength);
            double k = 1.2;
            double docLengthWeight = docLength / avgDocLength;

            topicScore = helper.calculateTopicScore(words, doc, k, bVA, docLengthWeight);
            result.setScore(topicScore);

            scoreResults.add(result);
        }


        endTime = new Date().getTime();
        return scoreResults;
    }

    private double calculateBVA(double mavgtf, double ld, double avgDocLength, int docLength) {
        return Math.pow(mavgtf, -2) * ld/docLength + (1 - Math.pow(mavgtf, -1)) * ld / avgDocLength;
    }

    @Override
    public long getDuration() {
        return endTime - startTime;
    }
}
