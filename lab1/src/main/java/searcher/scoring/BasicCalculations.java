package searcher.scoring;

import index.Index;

/**
 * Created by cbednar on 23.04.2017.
 */
public class BasicCalculations {

    private Index index;

    public BasicCalculations(Index index) {
        this.index = index;
    }

    /**
     * Term Frequency also known as TF measures the number of times a term (word) occurs in a document
     *
     * @param t term (or word)
     * @param d documentId
     * @return
     */
    public int tf(String t, String d) {
        return index.getFrequenciesOfWordInDocument(t, d);
    }

    /**
     * on different document sizes tf needs to be normalized otherwise longer documents are better rated
     *
     * @param t term
     * @param d documentId
     * @return
     */
    public double tfNormalized(String t, String d) {
        double tf = (double) tf(t, d);
        double docLength = (double) index.getDocumentLength(d);
        return  tf/docLength ;
    }

    /**
     * @param t term
     * @return
     */
    public double idf(String t) {
        double nrOfDocumentsContainingWord = (double) index.getNumberOfDocumentsContainingWord(t);
        if(nrOfDocumentsContainingWord >= 0) {
            double totalDocCount = (double) index.getTotalDocumentCount();
            return 1 + Math.log(totalDocCount/nrOfDocumentsContainingWord);
        } else {
            return 1;
        }
    }
}
