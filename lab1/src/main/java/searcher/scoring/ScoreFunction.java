package searcher.scoring;


import index.Index;
import parsing.trec.TrecTopic;

import java.util.List;
import java.util.Set;

public interface ScoreFunction {

    List<ScoreResult> getScoreResults(int topicId, Set<String> query);

    long getDuration();
}
