package searcher.scoring;

import index.Index;

import java.util.HashSet;
import java.util.Set;

public class ScoreHelper {

    private Index index;

    public ScoreHelper(Index index) {
        this.index = index;
    }

    public Set<String> getValidDocuments(Set<String> normalizedTokens) {
        Set<String> validDocuments = new HashSet<>();
        for (String word : normalizedTokens) {
            if (word != null) {
                Set<String> docs = index.getDocumentsContainingWord(word);
                if (docs != null && docs.size() != 0)
                    validDocuments.addAll(docs);
            }
        }
        return validDocuments;
    }


    private float score(float idfWeight, int wordFrequencies, double k, double b, double docLengthWeight) {

        double counter = wordFrequencies * (k + 1);
        double denominator = wordFrequencies + k * (1 - b + b * docLengthWeight);

        return (float) (idfWeight * (counter / denominator));
    }


    private float calculateInverseDocumentFrequencyWeight(Index index, String term) {
        int numberOfDocumentsContainingWord = index.getNumberOfDocumentsContainingWord(term);
        return (float) Math.log((index.getTotalDocumentCount() - numberOfDocumentsContainingWord + 0.5) / (numberOfDocumentsContainingWord + 0.5));
    }

    public double calculateTopicScore(Set<String> normalizedTokens, String doc, double k, double b, double docLengthWeight) {
        double topicScore = 0;
        for (String word : normalizedTokens) {
            if (word != null) {
                float idfWeight = calculateInverseDocumentFrequencyWeight(index, word);
                int wordFrequencies = index.getFrequenciesOfWordInDocument(word, doc);
                float score = score(idfWeight, wordFrequencies, k, b, docLengthWeight);
                topicScore += score;
            }
        }
        return topicScore;
    }
}
