package searcher.scoring;

import index.Index;
import parsing.ContentParser;
import parsing.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class BestMatching25 implements ScoreFunction {

    private long startTime;
    private long endTime;

    private Index index;
    private ScoreHelper helper;


    public BestMatching25(Index index) {
        this.index = index;
        this.helper = new ScoreHelper(index);
    }

    @Override
    public List<ScoreResult> getScoreResults(int topicId, Set<String> words) {

        startTime = new Date().getTime();

        List<ScoreResult> scoreResults = new ArrayList<>();
        double avgDocLength = index.getAvgDocumentLength();

        Set<String> validDocuments = helper.getValidDocuments(words);

        for (String doc : validDocuments) {
            ScoreResult result = new ScoreResult();
            result.setTopicID(topicId);
            result.setDocumentID(doc);

            double topicScore;
            double b = 0.75;
            double k = 1.5;
            int docLength = index.getDocumentLength(doc);
            double docLengthWeight = docLength / avgDocLength;

            topicScore = helper.calculateTopicScore(words, doc, k, b, docLengthWeight);

            result.setScore(topicScore);

            scoreResults.add(result);
        }

        endTime = new Date().getTime();
        return scoreResults;
    }

    @Override
    public long getDuration() {
        return endTime - startTime;
    }
}
