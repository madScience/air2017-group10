package searcher.scoring;

import java.util.Comparator;

public class ScoreResult {

    /**
     * Integer number between 401 and 450
     */
    private int topicID;

    /**
     * Identifier for the document (e.q. LA122690-0033)
     */
    private String documentID;

    /**
     * Rank of the object in sorted list (normally, this should be ascending from
     * first line to last line)
     */
    private int rank;

    /**
     * Similarity score calculated by the respective scoring function
     */
    private double score;


    public int getTopicID() {
        return topicID;
    }

    public void setTopicID(int topicID) {
        this.topicID = topicID;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return topicID + " Q0 " + documentID + " " + rank + " " + score + " grp10-exp1";
    }

    public static class ScoreResultsComparator implements Comparator<ScoreResult> {
        @Override
        public int compare(ScoreResult o1, ScoreResult o2) {
            int c = Integer.compare(o1.getTopicID(), o2.getTopicID());
            if(c == 0) {
                return Double.compare(o1.getScore(), o2.getScore());
            }
            return c;
        }
    }

    public static class ScoreComparator implements Comparator<ScoreResult> {

        @Override
        public int compare(ScoreResult o1, ScoreResult o2) {
            if (o1.getScore() < o2.getScore()) {
                return 1;
            } else {
                if (o1.getScore() > o2.getScore()) {
                    return -1;
                }
            }
            return 0;
        }

    }


}
