package searcher;

import org.apache.commons.cli.*;

public class CommandLineHandling {

    public static final String INDEX = "index";

    public CommandLine parseArguments (Options options, String[] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            return line;
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("searcher", options);
            System.err.println("Parsing failed. Reason: " + e.getMessage());
            return null;
        }
    }
}
