import index.Index;
import index.IndexHandling;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import parsing.CommandLineHandling;
import parsing.ContentParser;
import parsing.normalizer.Normalizer;
import parsing.tokenizer.Tokenizer;
import parsing.trec.TrecDocument;
import parsing.trec.TrecDocumentParser;
import scala.Tuple2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Indexer Class that is used to generate the index of the trec document collection
 * Either mapping can be used or the standard indexing procedure.
 * For the map-reduce task to work apache spark has to be included as a system path variable (folder where the bin folder resides)
 * The hadoop winutils is included inside this class directly because it is only needed on computers running on windows and
 * the spark standalone does not require a full installation of hadoop on the same computer
 * Furthermore, when building the jar file, it has to be noted that the integrated hadoop/bin folder in the resources part of the source
 * won't always be included in the jar file or not found when the shell scripts are executed with the mapreduce argument
 */
public class Indexer {
    private static final Logger LOGGER = org.slf4j.LoggerFactory.getLogger(Indexer.class);

    private static final String OPTION_HELP = "help";
    private static final String OPTION_INPUT = "input";
    private static final String OPTION_OUTPUT = "output";

    private static Index index;

    public static void main(String[] args) {
        CommandLineHandling cliHandling = new CommandLineHandling();
        Options opts = createOptions();
        CommandLine line = cliHandling.parseArguments(opts, args);
        if (line == null) {
            return;
        }
        if (line.hasOption(OPTION_HELP)) {
            // automatically generate the help statement
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("indexer", opts);
            return;
        }

        TrecDocumentParser trecParser = new TrecDocumentParser();
        Tokenizer tokenizer = new Tokenizer();
        List<Normalizer> normalizers = cliHandling.defineNormalizers(line);
        ContentParser contentParser = new ContentParser(tokenizer, normalizers);

        IndexHandling indexHandling = new IndexHandling();
        index = new Index(normalizers);

        String inputPath = line.getOptionValue(OPTION_INPUT);

        long totalDocumentCount = 0;

        if (line.hasOption(CommandLineHandling.MAPREDUCE)) {
            System.setProperty("hadoop.home.dir", Indexer.class.getResource("/hadoop").getPath());

            SparkConf conf = new SparkConf();
            conf.setAppName(Indexer.class.getName());
            conf.setMaster("local[*]");
            conf.set("spark.sql.warehouse.dir", "file:///c:/tmp/spark-warehouse");
            conf.set("mapreduce.input.fileinputformat.input.dir.recursive", "true");

            JavaSparkContext context = new JavaSparkContext(conf);

            JavaPairRDD<String, String> mr_files = context.wholeTextFiles(inputPath);
            JavaRDD<Integer> docCounts = mr_files.map((Function<Tuple2<String, String>, Integer>) fileNameContent -> {
                LOGGER.debug("Parsing file: " + fileNameContent._1());
                Integer documentCount = 0;
                List<TrecDocument> documents = trecParser.parse(Paths.get(fileNameContent._1().replaceFirst("file:/", "")));
                for (TrecDocument document : documents) {
                    documentCount++;
                    String text = document.getText();
                    List<String> tokensNormalized = contentParser.parse(text);
                    double tfSum = getDocumentTF(tokensNormalized, document, index);

                    index.addDocumentInfoExtended(document.getDocNo(), tokensNormalized.size(), tfSum);
                }
                return documentCount;
            });

            List<Integer> allCounts = docCounts.collect();
            for (Integer count : allCounts) {
                totalDocumentCount += count;
            }

            index.setTotalDocumentCount(totalDocumentCount);

        } else {
            try {
                List<Path> files = readFilesToParse(inputPath);
                assert files != null;
                for (Path file : files) {
                    LOGGER.debug("Parsing file: " + file.toString());
                    List<TrecDocument> documents = trecParser.parse(file);
                    totalDocumentCount += documents.size();

                    for (TrecDocument doc : documents) {
                        String text = doc.getText();
                        List<String> tokensNormalized = contentParser.parse(text);

                        double tfSum = getDocumentTF(tokensNormalized, doc, index);

                        index.addDocumentInfoExtended(doc.getDocNo(), tokensNormalized.size(), tfSum);
                    }
                }
                index.setTotalDocumentCount(totalDocumentCount);
            } catch (IOException e) {
                LOGGER.error("Wasn't able to read inputfile to create index and therefore stopped indexing", e);
            }
        }
        LOGGER.info("Read documentcount: " + totalDocumentCount);

        String outputFilePath = line.getOptionValue(OPTION_OUTPUT);
        LOGGER.debug("Storing index in file: " + outputFilePath);
        indexHandling.writeIndex(index, outputFilePath);
        LOGGER.debug("Finished creating index");
    }

    private static List<Path> readFilesToParse(String inputPath) {
        LOGGER.debug("Gathering files to create index from directory: " + inputPath);
        try {
            List<Path> files = Files.find(Paths.get(inputPath),
                    Integer.MAX_VALUE,
                    (filePath, fileAttr) -> fileAttr.isRegularFile()).collect(Collectors.toList());
            if (files == null || files.size() == 0) {
                LOGGER.info("No files found to parse in directory " + inputPath);
            }
            return files;
        } catch (IOException e) {
            LOGGER.error("Error on reading files", e);
            return null;
        }
    }

    private static Options createOptions() {
        Options options = new Options();
        options.addOption("h", "help", false, "prints this message");
        options.addRequiredOption("i", OPTION_INPUT, true, "the input directory which gets read recursively");
        options.addRequiredOption("o", OPTION_OUTPUT, true, "the filename where the Index should be stored");

        options.addOption("cf", CommandLineHandling.CASE_FOLDING, false, "Case-folding as normalization technique");
        options.addOption("rms", CommandLineHandling.REMOVE_STOP_WORDS, false, "Removing stopwords as normalization technique");
        options.addOption("l", CommandLineHandling.LEMMATIZE, false, "Using lemmatization as normalization technique");
        options.addOption("s", CommandLineHandling.STEMMING, false, "Using stemming as normalization technique");
        options.addOption("mapreduce", CommandLineHandling.MAPREDUCE, false, "Using map reduce functionality (if configured) to write index file");
        return options;
    }

    private static double getDocumentTF(List<String> tokensNormalized, TrecDocument document, Index index) {
        double tfSum = 0d;
        for (int i = 0; i < tokensNormalized.size(); i++) {
            String token = tokensNormalized.get(i);
            index.add(token, document.getDocNo(), Collections.frequency(tokensNormalized, token));
            int tf = Collections.frequency(tokensNormalized, token);
            tfSum += (double) tf / tokensNormalized.size();
        }
        return tfSum;
    }
}
