import com.google.common.io.Files;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.util.CoreMap;
import parsing.normalizer.PorterStemmer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;

import static java.nio.charset.StandardCharsets.UTF_8;

public class BasicPipelineExample {


    public static void main (String[] args) throws IOException {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        String text = readFile("src/main/resources/input.txt", UTF_8);

        Annotation document = pipeline.process(text);

        PorterStemmer stemmer = new PorterStemmer();

        for (CoreMap sentence: document.get(CoreAnnotations.SentencesAnnotation.class)) {
            for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);

                stemmer.add(word.toCharArray(), word.length());
                stemmer.stem();
                System.out.println("stemmed version of      " + word + " => " + stemmer.toString());

                System.out.println("lemmatized version of   " + word + " => " + lemma);
            }
        }
    }

    private static String readFile(String path, Charset encoding) throws IOException {
        return Files.toString(new File(path), encoding);
    }



    private static void stemmerExample(String path, Charset encoding, PorterStemmer stemmer) throws IOException {
        String words = Files.toString(new File(path), encoding);
        String[] words_array = words.split(" ");

        for (String word: words_array) {
            stemmer.add(word.toCharArray(), word.length());
            stemmer.stem();
            System.out.println("stemmed version of " + word + " => " + stemmer.toString());
        }
    }
}
