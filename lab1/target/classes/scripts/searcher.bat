@ECHO OFF
set CLASSPATH=.
set CLASSPATH=%CLASSPATH%;

REM set INDEXPATH=C:\git\air2017-group10\src\test\resources\temp\twofileindex.index
set INDEXPATH=C:\git\air2017-group10\src\test\resources\temp\allfileindex.index
set OUTPUTPATH=C:\git\air2017-group10\src\test\resources\temp\grp10-exp1-bm25

%JAVA_HOME%/bin/java -Xmx8g -jar ././../../../../target/air2017-group10-1.0-jar-with-dependencies.jar searcher -i %INDEXPATH% -f C:\git\air2017-group10\src\test\resources\topics\topicsTREC8Adhoc.txt -sf BM25 -o %OUTPUTPATH%