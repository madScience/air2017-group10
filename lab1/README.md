##Advanced Information Retrieval - Lab 01 - Group 10

This repository has been developed on computers using Windows / OSX, so the following steps have been tested and are needed to be done in order to compile and execute the program properly. The link to the executable jar is available via drive under the following link: https://drive.google.com/file/d/0B3ADL-yhUXPqRnNUR2pfUXJjS28/view?usp=sharing

### Preliminary steps ###

* The maven project comes with a lot of dependencies that are needed to create both indexer and searcher programs, so maven has to be installed and included in the classpath.
* The project was set up with Java Version 8.

### Additional requirements

* The project ships with two executable shell scripts, one for each of the main functionalities (searcher and indexer).
* The standard indexer can be used without further configurations, but when the mapreduce option is submitted as an argument, the system has to have a classpath variable set to the apache spark folder. 
* Apache Spark depends on hadoop functionalities, so a suitable .exe file (winutils.exe) is included inside the resources folder, which is referenced inside the mapreduce branch in the program and also set accordingly as a system property. 
* The indexer.sh and searcher.sh files can be executed. When wrong or missing arguments have been submitted, the program outputs the correct syntax of running the program properly.

### Indexer
usage: indexer
 -cf,--case-folding         Case-folding as normalization technique
 -h,--help                  prints this message
 -i,--input <arg>           the input directory which gets read
                            recursively
 -l,--lemmatize             Using lemmatization as normalization technique
 -mapreduce,--mapreduce     Using map reduce functionality (if configured)
                            to write index file
 -o,--output <arg>          the filename where the Index should be stored
 -rms,--remove-stop-words   Removing stopwords as normalization technique
 -s,--stem                  Using stemming as normalization technique

### Searcher
usage: searcher
 -f,--file <arg>                the input file containing the topic to
                                search for
 -h,--help                      prints help message
 -i,--index <arg>               file containing the previously generated
                                index
 -o,--output <arg>              outputs searchranking to this file
 -sf,--scoring_function <arg>   the scoring function that has to be used
                                by the engine (possible values are TF-IDF,
                                BM25 and BM25VA)